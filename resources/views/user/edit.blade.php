@extends('layouts.app')

@section('content')

<h2 class="text center col-md-offset-4  ">EDIT JADWAL MAIN BARENG</h2> 
<button> <a class="btn-succes offset-3" href="/home" >Home </a></button>
<div class="container">
        <form action="{{route('user.update', $transaksis )}}" method="post">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <div class="form-group">
                <label for="">Lokasi</label>
            <input type="text" class="form-control" name="lokasi" value="{{$transaksis->lokasi}}">
            </div>
             <div class="form-group">
                <label>Olahraga</label>
                <input type="text" class="form-control" name="olahraga"value="{{$transaksis->olahraga}}" >
            </div>             
            <div class="form-group">                    
                <input type="submit" class="btn btn-primary"  value="save">
            </div>
        </form>
    </div>



@endsection