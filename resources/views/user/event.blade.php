@extends('layouts.app')

@section('content')
 <div  class="text center col-md-offset-4  ">
     <h2>JADWAL MAIN BARENG</h2> 
     <button> <a class="btn-succes  " href="/home" >Home </a></button>
 </div>
@foreach ($transaksis as $transaksi)

<div class="container">
    
    <br><br>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
               
                    <div class="panel panel-default">
                    <div class="panel-heading text-center">Schedule</div>
                    <div class="panel-body">
                    <p>lokasi bertempat di lapangan " {{ $transaksi->lokasi }} "</p>
                    <p>dan olahraga yang diikutkan peserta yaitu "{{ $transaksi->olahraga }} "<p>

                    <a class="btn btn-danger" style="float:left">JOIN</a> 
                    <div>{{'&nbsp;'}}</div><div>{{'&nbsp;'}}</div>
                    <a class="btn btn-danger" style="float:left" href="{{url('user.update')}}">EDIT</a> 
                    <div>{{'&nbsp;'}}</div><div>{{'&nbsp;'}}</div>
                    <a>                       
                        <form action="{{route('user.destroy',  $transaksi)}}" method="POST">
                          {{ csrf_field() }}
                          {{ method_field('DELETE')}}
                            <button class="btn btn-danger "  type="submit">DELETE</button>
                          </form>
                    </a>                   
                    </div>
                </div>
            </div>       
        </div>
    </div>
        
@endforeach
@endsection