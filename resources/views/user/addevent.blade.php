@extends('layouts.app')

@section('content')

<h2 class="text center col-md-offset-3">TAMBAHKAN JADWAL MAIN BARENG</h2> 


<div class="container">
  <form action="{{route('user.store')}}" method="POST">
      {{ csrf_field() }}
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <button> <a class="btn-succes" href="/home" >Home </a></button>
                    <div class="panel panel-default">
                    <div class="panel-heading text-center">Add Schedule</div>
                    <div class="panel-body">
                        <div class="form-group">
                        <label>Lokasi</label>
                          <input type="text" class="form-control" name="lokasi" placeholder="Lokasi">
                        </div>
                        <div class="form-group">
                        <label>Jenis Olahraga</label>
                          <input type="text" class="form-control" name="olahraga" placeholder="jenis olahraga">
                        </div>
                        <div class="form-group">                    
                          <input type="submit" class="btn btn-primary"  value="save">
                        </div>          
                    </div>
                </div>
            </div>       
        </div>
        </form>
    </div>
@endsection