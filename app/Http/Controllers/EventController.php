<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Transaksi;
use App\Quotation;

class EventController extends Controller
{
    public function index()
    {

        // return view('user.event');
        $transaksis = Transaksi::latest()->paginate('5');
        //dd($items);
        return view('user.event', compact('transaksis'));
    }


    public function create()
    {
        
        return View ('user.addevent');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
        Transaksi::create([
            'lokasi' => request('lokasi'),
            'olahraga' => request('olahraga'),
            
        ]);

        return redirect('/home/event');

       
    }

    public function destroy($id)
    {
        //
        $transaksis = Transaksi::find($id);
        $transaksis->delete();

       return redirect()->name('user.event');
    }

    public function show($id)
    {
        //
        $transaksis = Transaksi::find($id);
        return view('user.showevent',compact('transaksis'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $transaksis = Transaksi::find($id);

        return view('user.edit',compact('transaksis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( $id)
    {
        //
        $transaksis= Transaksi::find($id);

        $transaksis->update([
            'lokasi' => request('lokasi'),    
            'olahraga' => request('olahraga')
            

        ]);

          return redirect('home/event');
    }

}
